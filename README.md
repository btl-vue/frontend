## Kiến trúc cơ bản
- Vuejs: 3.0.
- Nodejs: v14.20.1
## Hướng dẫn clone dự án
- git clone https://gitlab.com/btl-vue/frontend.git
## Setup dự án trên local
- run **npm install**
- run **npm run serve**

### Setup file môi trường
- setup baseURL(link api) tại file **src/http-common.js**

## Tài khoản
- email, username sẽ là tên viết tắt của bạn vd: Phạm Thế Anh => username: Anhpt, email: anhpt@thefirstone.jp, password anhpt@tfo