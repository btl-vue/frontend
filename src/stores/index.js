import { login } from "../apis/auth"
import { ElNotification } from 'element-plus'

const storeConfigs = {
    state : {
      btn_loading : false
    },
    getters :{ 
      userLogin : state => state.userLogin,
      btnLoading : state => state.btn_loading,
    },
    mutations : {
      setUserLogin(state, user){
          state.userLogin = user
      },
      setBtnLoading(state, status){
        state.btn_loading = status
      },
    },
    actions : {
      updateUserLogin (context, payload){
        context.commit('setBtnLoading', true)
        login(payload)
          .then(res => {
            context.commit('setBtnLoading', false)
            localStorage.setItem('user_login', JSON.stringify(res.data.user))
            localStorage.setItem('user_id', res.data.user.id)
            localStorage.setItem('user_token', res.data.authorisation.token)
            localStorage.setItem('role_user', res.data.user.role)
            if (res.data.user.role == 2) {
              window.location.href = "/requests/products"
            }else {
              window.location.href = "/"
            }
          })
          .catch(e => {
            context.commit('setBtnLoading', false)
            if (e.response.status == 401) {
              ElNotification({
                title: 'Error',
                message: e.response.data.message,
                type: 'error',
              })
            }
            if (e.response.status == 422) {
              console.log(e)
              ElNotification({
                title: 'Error',
                message: e.response.data.message,
                type: 'error',
              })
            }
            
          })
      }
    }
  }

export default storeConfigs