import { createApp } from 'vue'
import { createStore } from 'vuex'
import router from "./router";
import App from './App.vue'
import 'element-plus/dist/index.css'
import ElementPlus from 'element-plus'
import ElementPlusLocaleVi from 'element-plus/dist/locale/vi'
import storeConfigs from './stores';

const store = createStore(storeConfigs)

const app = createApp(App)
app.use(store)
app.use(router)
app.use(ElementPlus, ElementPlusLocaleVi)

.mount('#app')

