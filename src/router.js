import { createRouter, createWebHistory } from "vue-router"
import Home from "./components/homes/index.vue"
import Login from "./components/auths/Login.vue"
import ResetPassword from "./components/auths/reset_password.vue"
import UserList from "./components/users/list.vue"
import UserCreate from "./components/users/create.vue"
import UserEdit from "./components/users/edit.vue"
import UserEditProfile from "./components/users/edit_profile.vue"
import DepartmentList from "./components/departments/list.vue"
import DepartmentCreate from "./components/departments/create.vue"
import DepartmentEdit from "./components/departments/edit.vue"
import CategoryList from "./components/categories/list.vue"
import ProductList from "./components/products/list.vue"
import ProductCreate from "./components/products/create.vue"
import ProductEdit from "./components/products/edit.vue"
import RequestProductList from "./components/requests/product.vue"
import RequestList from "./components/requests/list.vue"
import RequestListUser from "./components/requests/listUser.vue"
import RequestReturnProduct from "./components/requests/return_product.vue"
import RequestReturnProductUser from "./components/requests/return_product_user.vue"

const routes = [
  { path: "/", name: "home", component: Home,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/requests/products"
    }
  },

  //auth
  { path: "/login", name: "Login", component: Login },
  { path: "/reset-password", name: "ResetPassword", component: ResetPassword },
  //main
  
  { path: "/users", name: "UserList", component: UserList, 
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/users/create", name: "UserCreate", component: UserCreate,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/users/:id/edit", name: "UserEdit", component: UserEdit, 
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/users/:id/edit-profile", name: "UserEditProfile", component: UserEditProfile},
  //department
  { path: "/departments", name: "DepartmentList", component: DepartmentList,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/departments/create", name: "DepartmentCreate", component: DepartmentCreate,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/departments/:id/edit", name: "DepartmentEdit", component: DepartmentEdit,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  //category
  { path: "/categories", name: "CategoryList", component: CategoryList,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  //products
  { path: "/products", name: "ProductList", component: ProductList,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/products/create", name: "ProductCreate", component: ProductCreate,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/products/:id/edit", name: "ProductEdit", component: ProductEdit,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  //request
  { path: "/requests", name: "RequestListUser", component: RequestListUser},
  { path: "/requests/products", name: "RequestProductList", component: RequestProductList},
  { path: "/requests/lists", name: "RequestList", component: RequestList,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/requests/return-product", name: "RequestReturnProduct", component: RequestReturnProduct,
    beforeEnter: () => {
      if(localStorage.getItem('role_user') == 1){
        return true
      }
      return "/"
    }
  },
  { path: "/requests/return-product-user", name: "RequestReturnProductUser", component: RequestReturnProductUser},
];

const router = createRouter({
  history: createWebHistory(),
  routes : routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || new Promise((resolve)=>{
      setTimeout(()=> resolve({ top:0 }), 500)
    })
  }
});
export default router;

