import axios from "axios";

export function login(credential) {
    return new Promise((res) => {
        axios.post('/api/auth/login', credential)
            .then(result => {
                res(result.data);
            })
    })
}

export function register(credential) {
    return new Promise((res) => {
        axios.post('/api/auth/register', credential)
            .then(result => {
                res(result.data);
            })
    })
}

export function currentUser() {
    const user = localStorage.getItem('user');

    if (!user) {
        return null;
    }

    return JSON.parse(user);
}