import { Api } from "../http-common";

export const request = (page, data) => {
  const url = `/requests/all?page=` + page;
  return Api.post(url, data)
};

export const requestUser = (page, data) => {
  const url = `/requests/all?page=` + page;
  return Api.post(url, data)
};

export const requestGetAllApprove = (page) => {
  const url = `/requests/approved?page=` + page;
  return Api.get(url)
};

export const requestFind = (id) => {
  const url = `/requests/` + id;
  return Api.get(url)
};

export const requestApproveMultiple = (data) => {
  const url = `/approved-request-multiple`;
  return Api.post(url, data)
};

export const requestSend = (data) => {
  const url = `/requests`;
  return Api.post(url, data)
};

export const requestReject = (data) => {
  const url = `/requests/rejected`;
  return Api.put(url, data)
};

export const requestApprove = (data) => {
  const url = `/requests/approved`;
  return Api.put(url, data)
};

export const getLink = (data) => {
  const url = `/requests/export-url`;
  return Api.post(url, data)
};

export const returnProduct = (data) => {
  const url = `/requests/returnted`;
  return Api.post(url, data)
};

export const updateStatusSingle = (data, id) => {
  const url = `my-requests/update-status-requests/`+id;
  return Api.put(url, data)
};

export const requestReturnGetAll = (page, user_id) => {
  const url = `requests/returnt-data?page=`+ page + `&user=` + user_id;
  return Api.get(url)
};

export const requestReturnDataReject = (data) => {
  const url = `requests/returnt-data-reject`
  return Api.post(url, data)
};

export const requestDataProduct = (id) => {
  const url = `requests/return-data-product/`+id
  return Api.get(url)
};


