import { Api } from "../http-common";

export const departments = () => {
  const url = `datas/departments`;
  return Api.get(url)
};

export const categories = () => {
  const url = `datas/categories`;
  return Api.get(url)
};

export const products = () => {
  const url = `datas/products`;
  return Api.get(url)
};

export const users = () => {
  const url = `datas/users`;
  return Api.get(url)
};

export const requests = () => {
  const url = `datas/requests`;
  return Api.get(url)
};

export const home = () => {
  const url = `datas/home`;
  return Api.get(url)
};

