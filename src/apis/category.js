import { Api } from "../http-common";

export const getAll = () => {
  const url = `/data-categories`;
  return Api.get(url)
};

export const categories = (page) => {
  const url = `/categories?page=` + page;
  return Api.get(url)
};

export const categoryCreate = (data) => {
    const url = `/categories`;
    return Api.post(url, data)
  };

export const categoryFind = (id) => {
    const url = `/categories/` + id;
    return Api.get(url)
};

export const categorytDelete = (id) => {
    const url = `/categories/` +id;
    return Api.delete(url)
};
export const categoryEdit = (id, data) => {
  const url = `/categories/` + id;
  return Api.put(url, data)
};
