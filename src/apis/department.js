import { Api } from "../http-common";

export const departments = (page) => {
  const url = `/departments?page=` + page;
  return Api.get(url)
};

export const dataDepartments = () => {
  const url = `data-departments`;
  return Api.get(url)
};

export const departmentCreate = (data) => {
    const url = `/departments`;
    return Api.post(url, data)
  };

export const departmentFind = (id) => {
    const url = `/departments/` + id;
    return Api.get(url)
};

export const departmentDelete = (id) => {
  const url = `/departments/` +id;
  return Api.delete(url)
};

export const departmentEdit = (id, data) => {
  const url = `/departments/` + id;
  return Api.put(url, data)
};