import { Api } from "../http-common";

export const users = (page, query) => {
  const url = `/users?page=` + page + `&keyword=` + query.keyword + `&role=` + query.role + `&department=` + query.department + `&status=` + query.status;
  return Api.get(url)
};

export const userCreate = (data) => {
    const url = `/users`;
    return Api.post(url, data)
};

export const userDelete = (id) => {
    const url = `/users/` + id;
    return Api.delete(url)
};

export const userFind = (id) => {
    const url = `/users/` + id;
    return Api.get(url)
};

export const userEdit = (id, data) => {
    const url = `/users/` + id;
    return Api.put(url, data)
  };

export const userAll = (page) => {
    const url = `/users?page=` + page;
    return Api.get(url)
};

export const userEditProfile = (data) => {
    const url = `/users/update-profile`;
    return Api.put(url, data)
  };

export const autoResetPassword = (id) => {
    const url = `/users/reset-password/`+id;
    return Api.get(url)
};
