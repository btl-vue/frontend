import { Api } from "../http-common";

export const products = (page, query) => {
  const url = `/products?page=` + page + `&keyword=` + query.keyword + `&category=` + query.category;
  return Api.get(url)
};

export const productCreate = (data) => {
  const url = `/products`;
  return Api.post(url, data)
};

export const productEdit = (id, data) => {
  const url = `/products/` + id;
  return Api.put(url, data)
};

export const productFind = (id) => {
    const url = `/products/` + id;
    return Api.get(url)
};

export const productDelete = (id) => {
    const url = `/products/` +id;
    return Api.delete(url)
  }