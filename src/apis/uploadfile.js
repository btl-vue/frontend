import { Api } from "../http-common";

export const products = (data) => {
    const url = `/upload-image-product`;
    return Api.post(url, data)
};

export const productExcel = (data) => {
    const url = `/upload-excel`;
    return Api.post(url, data)
};

export const categoriesUpload = (data) => {
    const url = `/upload-image-categories`;
    return Api.post(url, data)
};

export const productsDeleteImageEdit = (data) => {
    const url = `/delete-image-product`;
    return Api.post(url, data)
};

export const requestExcelImport = (data) => {
    const url = `/upload-excel-request`;
    return Api.post(url, data)
};
