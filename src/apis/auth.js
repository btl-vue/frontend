import { Api } from "../http-common";

export const login = (data) => {
  const url = `/login`;
  return Api.post(url, data)
};

export const logout = () => {
  const url = `/logout`;
  return Api.post(url)
};

export const resetPassword = (data) => {
  const url = `/users/forgot-password`;
  return Api.put(url, data)
};